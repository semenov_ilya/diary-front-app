var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function ModalWindow(props) {
		return React.createElement(
				"div",
				{ className: "modal fade", id: "infoModal", tabIndex: "-1", "aria-labelledby": "exampleModalLabel", "aria-hidden": "true" },
				React.createElement(
						"div",
						{ className: "modal-dialog", role: "document" },
						React.createElement(
								"div",
								{ className: "modal-content rounded-4 shadow" },
								React.createElement(
										"div",
										{ className: "modal-body p-4" },
										React.createElement(
												"h5",
												{ className: "mb-2 text-center" },
												"\u0414\u043E\u0431\u0430\u0432\u043B\u0435\u043D\u0438\u0435 telegram \u0430\u043A\u043A\u0430\u0443\u043D\u0442\u0430"
										),
										React.createElement(
												"ol",
												null,
												React.createElement(
														"li",
														null,
														"\u0414\u043B\u044F \u0432\u0430\u0441 \u0431\u044B\u043B \u0441\u0433\u0435\u043D\u0435\u0440\u0438\u0440\u043E\u0432\u0430\u043D \u043A\u043E\u0434 ",
														props.code
												),
												React.createElement(
														"li",
														null,
														"\u041E\u0442\u043A\u0440\u043E\u0439\u0442\u0435 \u043F\u0440\u0438\u043B\u043E\u0436\u0435\u043D\u0438\u0435 \u0442\u0435\u043B\u0435\u0433\u0440\u0430\u043C"
												),
												React.createElement(
														"li",
														null,
														"\u041D\u0430\u0439\u0434\u0438\u0442\u0435 @DairyBotBot"
												),
												React.createElement(
														"li",
														null,
														"\u041D\u0430\u043F\u0438\u0448\u0438\u0442\u0435 \u0431\u043E\u0442\u0443 @DairyBotBot \u0441\u043E\u043E\u0431\u0449\u0435\u043D\u0438\u0435 /reg \u041A\u041E\u0414"
												),
												React.createElement(
														"cite",
														{ title: "Source Title" },
														"\u041F\u0440\u0438\u043C\u0435\u0440: /reg ABCD"
												),
												React.createElement(
														"li",
														null,
														"\u0412\u0430\u043C \u043F\u0440\u0438\u0434\u0435\u0442 \u0443\u0432\u0435\u0434\u043E\u043C\u043B\u0435\u043D\u0438\u0435 \u043E\u0431 \u0443\u0441\u043F\u0435\u0448\u043D\u043E\u0439 \u043F\u0440\u0438\u0432\u044F\u0437\u043A\u0435"
												)
										)
								),
								React.createElement(
										"div",
										{ className: "modal-footer flex-nowrap p-0" },
										React.createElement(
												"button",
												{ type: "button", className: "btn btn-lg btn-link fs-6 text-decoration-none col-12 m-0 rounded-0", "data-bs-dismiss": "modal" },
												"\u0413\u043E\u0442\u043E\u0432\u043E"
										)
								)
						)
				)
		);
}

function LinkTelegramButton(props) {
		return React.createElement(
				"div",
				{ className: "col-sm-6" },
				React.createElement(
						"button",
						{ type: "button", className: "btn btn-primary", onClick: props.settingsFormObj.linkTelegram },
						React.createElement(
								"svg",
								{ xmlns: "http://www.w3.org/2000/svg", width: "16", height: "16", fill: "currentColor", className: "bi bi-telegram", viewBox: "0 0 16 16" },
								React.createElement("path", { d: "M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.287 5.906c-.778.324-2.334.994-4.666 2.01-.378.15-.577.298-.595.442-.03.243.275.339.69.47l.175.055c.408.133.958.288 1.243.294.26.006.549-.1.868-.32 2.179-1.471 3.304-2.214 3.374-2.23.05-.012.12-.026.166.016.047.041.042.12.037.141-.03.129-1.227 1.241-1.846 1.817-.193.18-.33.307-.358.336a8.154 8.154 0 0 1-.188.186c-.38.366-.664.64.015 1.088.327.216.589.393.85.571.284.194.568.387.936.629.093.06.183.125.27.187.331.236.63.448.997.414.214-.02.435-.22.547-.82.265-1.417.786-4.486.906-5.751a1.426 1.426 0 0 0-.013-.315.337.337 0 0 0-.114-.217.526.526 0 0 0-.31-.093c-.3.005-.763.166-2.984 1.09z" })
						),
						"\u041F\u0440\u0438\u0432\u044F\u0437\u0430\u0442\u044C"
				)
		);
}

function TelegramLinkedMessage() {
		return React.createElement(
				"div",
				{ className: "col-sm-6" },
				React.createElement(
						"svg",
						{ xmlns: "http://www.w3.org/2000/svg", width: "16", height: "16", fill: "currentColor", className: "bi bi-check-circle text-success", viewBox: "0 0 16 16" },
						React.createElement("path", { d: "M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" }),
						React.createElement("path", { d: "M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z" })
				),
				React.createElement(
						"span",
						{ className: "text-success" },
						"\u0410\u043A\u043A\u0430\u0443\u043D\u0442 \u043F\u0440\u0438\u0432\u044F\u0437\u0430\u043D"
				)
		);
}

function SvgSettings() {
		return React.createElement(
				"svg",
				{ xmlns: "http://www.w3.org/2000/svg", width: "16", height: "16", fill: "currentColor", className: "bi bi-tools me-2", viewBox: "0 0 16 16" },
				React.createElement("path", { d: "M1 0 0 1l2.2 3.081a1 1 0 0 0 .815.419h.07a1 1 0 0 1 .708.293l2.675 2.675-2.617 2.654A3.003 3.003 0 0 0 0 13a3 3 0 1 0 5.878-.851l2.654-2.617.968.968-.305.914a1 1 0 0 0 .242 1.023l3.356 3.356a1 1 0 0 0 1.414 0l1.586-1.586a1 1 0 0 0 0-1.414l-3.356-3.356a1 1 0 0 0-1.023-.242L10.5 9.5l-.96-.96 2.68-2.643A3.005 3.005 0 0 0 16 3c0-.269-.035-.53-.102-.777l-2.14 2.141L12 4l-.364-1.757L13.777.102a3 3 0 0 0-3.675 3.68L7.462 6.46 4.793 3.793a1 1 0 0 1-.293-.707v-.071a1 1 0 0 0-.419-.814L1 0zm9.646 10.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708zM3 11l.471.242.529.026.287.445.445.287.026.529L5 13l-.242.471-.026.529-.445.287-.287.445-.529.026L3 15l-.471-.242L2 14.732l-.287-.445L1.268 14l-.026-.529L1 13l.242-.471.026-.529.445-.287.287-.445.529-.026L3 11z" })
		);
}

function SvgCalendar() {
		return React.createElement(
				"svg",
				{ xmlns: "http://www.w3.org/2000/svg", width: "16", height: "16", fill: "currentColor", className: "bi bi-calendar-check me-2", viewBox: "0 0 16 16" },
				React.createElement("path", { d: "M10.854 7.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 9.793l2.646-2.647a.5.5 0 0 1 .708 0z" }),
				React.createElement("path", { d: "M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" })
		);
}

var MenuItem = function (_React$Component) {
		_inherits(MenuItem, _React$Component);

		function MenuItem(props) {
				_classCallCheck(this, MenuItem);

				var _this = _possibleConstructorReturn(this, (MenuItem.__proto__ || Object.getPrototypeOf(MenuItem)).call(this, props));

				_this.handleClick = _this.handleClick.bind(_this);
				return _this;
		}

		_createClass(MenuItem, [{
				key: "handleClick",
				value: function handleClick() {
						this.props.handler(this.props.menuName);
				}
		}, {
				key: "render",
				value: function render() {
						return React.createElement(
								"a",
								{ href: "#", onClick: this.handleClick, className: this.props.linkType },
								this.props.menuImage,
								this.props.menuName
						);
				}
		}]);

		return MenuItem;
}(React.Component);

var Navigation = function (_React$Component2) {
		_inherits(Navigation, _React$Component2);

		function Navigation(props) {
				_classCallCheck(this, Navigation);

				var _this2 = _possibleConstructorReturn(this, (Navigation.__proto__ || Object.getPrototypeOf(Navigation)).call(this, props));

				_this2.state = {
						activeMenuItem: "Ежедневник"
				};
				_this2.handler = _this2.handler.bind(_this2);
				_this2.handleLogout = _this2.handleLogout.bind(_this2);
				return _this2;
		}

		_createClass(Navigation, [{
				key: "handler",
				value: function handler(menuName) {
						this.setState({ activeMenuItem: menuName });
						this.props.handler(menuName);
				}
		}, {
				key: "handleLogout",
				value: function handleLogout() {
						logout();
				}
		}, {
				key: "render",
				value: function render() {
						return React.createElement(
								"div",
								{ className: "d-flex flex-column flex-shrink-0 p-3 bg-light navMenuDiv" },
								React.createElement(
										"a",
										{ href: "/", className: "d-flex align-items-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none" },
										React.createElement("img", { src: "./img/logo.svg", width: "40", height: "32" }),
										React.createElement(
												"span",
												{ className: "fs-4 ps-2" },
												"SDiary"
										)
								),
								React.createElement("hr", null),
								React.createElement(
										"ul",
										{ className: "nav nav-pills flex-column mb-auto" },
										React.createElement(
												"li",
												{ className: "nav-item" },
												React.createElement(MenuItem, { menuName: "\u041D\u0430\u0441\u0442\u0440\u043E\u0439\u043A\u0438", linkType: "Настройки" === this.state.activeMenuItem ? "nav-link active" : "nav-link link-dark", menuImage: React.createElement(SvgSettings, null), handler: this.handler })
										),
										React.createElement(
												"li",
												{ className: "nav-item" },
												React.createElement(MenuItem, { menuName: "\u0415\u0436\u0435\u0434\u043D\u0435\u0432\u043D\u0438\u043A", linkType: "Ежедневник" === this.state.activeMenuItem ? "nav-link active" : "nav-link link-dark", menuImage: React.createElement(SvgCalendar, null), handler: this.handler })
										)
								),
								React.createElement("hr", null),
								React.createElement(
										"div",
										null,
										React.createElement(
												"button",
												{ type: "button", className: "btn btn-primary btn-sm me-1", title: "\u0412\u044B\u0445\u043E\u0434", onClick: this.handleLogout },
												React.createElement(
														"svg",
														{ xmlns: "http://www.w3.org/2000/svg", width: "16", height: "16", fill: "currentColor", className: "bi bi-box-arrow-left", viewBox: "0 0 16 16" },
														React.createElement("path", { fillRule: "evenodd", d: "M6 12.5a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-8a.5.5 0 0 0-.5.5v2a.5.5 0 0 1-1 0v-2A1.5 1.5 0 0 1 6.5 2h8A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-8A1.5 1.5 0 0 1 5 12.5v-2a.5.5 0 0 1 1 0v2z" }),
														React.createElement("path", { fillRule: "evenodd", d: "M.146 8.354a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L1.707 7.5H10.5a.5.5 0 0 1 0 1H1.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3z" })
												)
										),
										React.createElement(
												"strong",
												null,
												this.props.activeUserName
										)
								)
						);
				}
		}]);

		return Navigation;
}(React.Component);

function NavigationDivider() {
		return React.createElement("div", { className: "b-example-divider" });
}

var PrimaryContentHeader = function (_React$Component3) {
		_inherits(PrimaryContentHeader, _React$Component3);

		function PrimaryContentHeader(props) {
				_classCallCheck(this, PrimaryContentHeader);

				return _possibleConstructorReturn(this, (PrimaryContentHeader.__proto__ || Object.getPrototypeOf(PrimaryContentHeader)).call(this, props));
		}

		_createClass(PrimaryContentHeader, [{
				key: "render",
				value: function render() {
						return React.createElement(
								"div",
								{ className: "mt-3" },
								React.createElement(
										"span",
										{ className: "fs-4" },
										this.props.activeMenuItem
								)
						);
				}
		}]);

		return PrimaryContentHeader;
}(React.Component);

function AlertDiv() {
		return React.createElement(
				"div",
				{ className: "settings mt-3" },
				React.createElement("div", { id: "alertDiv" })
		);
}

var SettingsForm = function (_React$Component4) {
		_inherits(SettingsForm, _React$Component4);

		function SettingsForm(props) {
				_classCallCheck(this, SettingsForm);

				var _this4 = _possibleConstructorReturn(this, (SettingsForm.__proto__ || Object.getPrototypeOf(SettingsForm)).call(this, props));

				_this4.state = {
						lastName: "",
						firstName: "",
						secondName: "",
						telegram: "",
						code: ""
				};
				_this4.linkTelegram = _this4.linkTelegram.bind(_this4);
				return _this4;
		}

		_createClass(SettingsForm, [{
				key: "linkTelegram",
				value: function linkTelegram() {
						var userInfo = getUserInfo();
						if (userInfo === null) {
								return;
						}

						if (userInfo.telegramChatId != null) {
								showAlert("Аккаунт уже привязан к telegram", "success");
								this.setState({ telegram: React.createElement(TelegramLinkedMessage, null) });
								return;
						}

						var telegramCode = generateTelegramCode(userInfo.userId);
						if (telegramCode === null) {
								return;
						}

						this.setState({ code: telegramCode });
						var myModal = new bootstrap.Modal(document.getElementById('infoModal'), {
								keyboard: false
						});
						myModal.show();
				}
		}, {
				key: "componentDidMount",
				value: function componentDidMount() {
						var settingsFormObj = this;

						var userInfo = getUserInfo();
						if (userInfo != null) {
								var telegram = null;
								if (userInfo.telegramChatId === null) {
										telegram = React.createElement(LinkTelegramButton, { settingsFormObj: this });
								} else {
										telegram = React.createElement(TelegramLinkedMessage, null);
								}

								this.setState({
										lastName: userInfo.lastName,
										firstName: userInfo.firstName,
										secondName: userInfo.secondName,
										telegram: telegram
								});
						}

						$("#settingsForm").validate({
								rules: {
										lastName: "required",
										firstName: "required"
								},
								messages: {
										lastName: "Введите фамилию",
										firstName: "Введите имя"
								},
								errorElement: "em",
								errorPlacement: function errorPlacement(error, element) {
										element.addClass("is-invalid");
										error.addClass("invalid-feedback");
										error.insertAfter(element);
								},
								highlight: function highlight(element, errorClass, validClass) {
										$(element).addClass("is-invalid").removeClass("is-valid");
								},
								unhighlight: function unhighlight(element, errorClass, validClass) {
										$(element).addClass("is-valid").removeClass("is-invalid");
								},
								submitHandler: function submitHandler(form) {
										var userInfo = getUserInfo();
										if (userInfo === null) {
												return;
										}
										var user = $(form).serializeJSON();
										emptyToNull(user);
										var mergedUserInfo = Object.assign(userInfo, user);
										updateUser(mergedUserInfo);
										settingsFormObj.props.handler($(form).find('input[name="lastName"]').val() + " " + $(form).find('input[name="firstName"]').val());
								}
						});

						$("#passwordChangeForm").validate({
								rules: {
										password: "required",
										passwordConfirm: {
												required: true,
												equalTo: "#password"
										}
								},
								messages: {
										password: "Введите пароль",
										passwordConfirm: {
												required: "Подтвердите ввод пароля",
												equalTo: "Пароли должны совпадать"
										}
								},
								errorElement: "em",
								errorPlacement: function errorPlacement(error, element) {
										element.addClass("is-invalid");
										error.addClass("invalid-feedback");
										error.insertAfter(element);
								},
								highlight: function highlight(element, errorClass, validClass) {
										$(element).addClass("is-invalid").removeClass("is-valid");
								},
								unhighlight: function unhighlight(element, errorClass, validClass) {
										$(element).addClass("is-valid").removeClass("is-invalid");
								},
								submitHandler: function submitHandler(form) {
										var userInfo = getUserInfo();
										if (userInfo === null) {
												return;
										}
										var mergedUserInfo = Object.assign(userInfo, $(form).serializeJSON());
										delete mergedUserInfo.passwordConfirm;
										updateUser(mergedUserInfo);
								}
						});
				}
		}, {
				key: "render",
				value: function render() {
						return React.createElement(
								"div",
								{ className: "row" },
								React.createElement(ModalWindow, { code: this.state.code }),
								React.createElement(
										"div",
										{ className: "settings mt-3" },
										React.createElement(
												"form",
												{ id: "settingsForm" },
												React.createElement(
														"div",
														{ className: "row mb-2" },
														React.createElement(
																"label",
																{ className: "col-sm-4 col-form-label", htmlFor: "lastName" },
																"\u0424\u0430\u043C\u0438\u043B\u0438\u044F"
														),
														React.createElement(
																"div",
																{ className: "col-sm-6" },
																React.createElement("input", { type: "text", className: "form-control", name: "lastName", defaultValue: this.state.lastName })
														)
												),
												React.createElement(
														"div",
														{ className: "row mb-2" },
														React.createElement(
																"label",
																{ className: "col-sm-4 col-form-label", htmlFor: "firstName" },
																"\u0418\u043C\u044F"
														),
														React.createElement(
																"div",
																{ className: "col-sm-6" },
																React.createElement("input", { type: "text", className: "form-control", name: "firstName", defaultValue: this.state.firstName })
														)
												),
												React.createElement(
														"div",
														{ className: "row mb-2" },
														React.createElement(
																"label",
																{ className: "col-sm-4 col-form-label", htmlFor: "secondName" },
																"\u041E\u0442\u0447\u0435\u0441\u0442\u0432\u043E"
														),
														React.createElement(
																"div",
																{ className: "col-sm-6" },
																React.createElement("input", { type: "text", className: "form-control", name: "secondName", defaultValue: this.state.secondName })
														)
												),
												React.createElement(
														"div",
														{ className: "row mb-2" },
														React.createElement(
																"label",
																{ className: "col-sm-4 col-form-label" },
																"Telegram \u0430\u043A\u043A\u0430\u0443\u043D\u0442"
														),
														this.state.telegram
												),
												React.createElement(
														"div",
														{ className: "text-center" },
														React.createElement(
																"button",
																{ type: "submit", className: "btn btn-primary" },
																"\u0418\u0437\u043C\u0435\u043D\u0438\u0442\u044C \u0434\u0430\u043D\u043D\u044B\u0435"
														)
												)
										)
								),
								React.createElement(
										"div",
										{ className: "settings mt-3" },
										React.createElement(
												"form",
												{ id: "passwordChangeForm" },
												React.createElement(
														"div",
														{ className: "row mb-2" },
														React.createElement(
																"label",
																{ className: "col-sm-4 col-form-label", htmlFor: "password" },
																"\u041D\u043E\u0432\u044B\u0439 \u043F\u0430\u0440\u043E\u043B\u044C"
														),
														React.createElement(
																"div",
																{ className: "col-sm-6" },
																React.createElement("input", { type: "password", className: "form-control", name: "password", id: "password" })
														)
												),
												React.createElement(
														"div",
														{ className: "row mb-2" },
														React.createElement(
																"label",
																{ className: "col-sm-4 col-form-label", htmlFor: "passwordConfirm" },
																"\u041F\u043E\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043D\u0438\u0435 \u043F\u0430\u0440\u043E\u043B\u044F"
														),
														React.createElement(
																"div",
																{ className: "col-sm-6" },
																React.createElement("input", { type: "password", className: "form-control", name: "passwordConfirm" })
														)
												),
												React.createElement(
														"div",
														{ className: "text-center" },
														React.createElement(
																"button",
																{ type: "submit", className: "btn btn-primary" },
																"\u0418\u0437\u043C\u0435\u043D\u0438\u0442\u044C \u043F\u0430\u0440\u043E\u043B\u044C"
														)
												)
										)
								)
						);
				}
		}]);

		return SettingsForm;
}(React.Component);

var DatePicker = function (_React$Component5) {
		_inherits(DatePicker, _React$Component5);

		function DatePicker(props) {
				_classCallCheck(this, DatePicker);

				var _this5 = _possibleConstructorReturn(this, (DatePicker.__proto__ || Object.getPrototypeOf(DatePicker)).call(this, props));

				_this5.state = {
						currentDate: new Date().toDateInputValue()
				};
				_this5.handleChangeDateClick = _this5.handleChangeDateClick.bind(_this5);
				_this5.handleChange = _this5.handleChange.bind(_this5);
				return _this5;
		}

		_createClass(DatePicker, [{
				key: "handleChange",
				value: function handleChange() {
						this.setState({ currentDate: event.target.value });
						this.props.handler(event.target.value);
				}
		}, {
				key: "handleChangeDateClick",
				value: function handleChangeDateClick(days) {
						var date = new Date(this.state.currentDate);
						date.setDate(date.getDate() + days);
						this.setState({ currentDate: date.toDateInputValue() });
						this.props.handler(date.toDateInputValue());
				}
		}, {
				key: "render",
				value: function render() {
						var _this6 = this;

						return React.createElement(
								"div",
								{ className: "date-picker input-group mt-3" },
								React.createElement(
										"button",
										{ className: "btn btn-primary", type: "button", onClick: function onClick() {
														return _this6.handleChangeDateClick(-1);
												} },
										React.createElement(
												"svg",
												{ xmlns: "http://www.w3.org/2000/svg", width: "16", height: "16", fill: "currentColor", className: "bi bi-arrow-left", viewBox: "0 0 16 16" },
												React.createElement("path", { fillRule: "evenodd", d: "M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z" })
										)
								),
								React.createElement("input", { type: "date", className: "form-control", value: this.state.currentDate, onChange: this.handleChange }),
								React.createElement(
										"button",
										{ className: "btn btn-primary", type: "button", onClick: function onClick() {
														return _this6.handleChangeDateClick(1);
												} },
										React.createElement(
												"svg",
												{ xmlns: "http://www.w3.org/2000/svg", width: "16", height: "16", fill: "currentColor", className: "bi bi-arrow-right", viewBox: "0 0 16 16" },
												React.createElement("path", { fillRule: "evenodd", d: "M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" })
										)
								)
						);
				}
		}]);

		return DatePicker;
}(React.Component);

var EventEntityRow = function (_React$Component6) {
		_inherits(EventEntityRow, _React$Component6);

		function EventEntityRow(props) {
				_classCallCheck(this, EventEntityRow);

				var _this7 = _possibleConstructorReturn(this, (EventEntityRow.__proto__ || Object.getPrototypeOf(EventEntityRow)).call(this, props));

				_this7.handleFinishEvent = _this7.handleFinishEvent.bind(_this7);
				_this7.handleCancelEvent = _this7.handleCancelEvent.bind(_this7);
				_this7.handleDeleteEvent = _this7.handleDeleteEvent.bind(_this7);
				return _this7;
		}

		_createClass(EventEntityRow, [{
				key: "handleFinishEvent",
				value: function handleFinishEvent(eventId) {
						var isSuccess = finishEvent(eventId);
						if (isSuccess === true) {
								this.props.handler();
						}
				}
		}, {
				key: "handleCancelEvent",
				value: function handleCancelEvent(eventId) {
						var isSuccess = cancelEvent(eventId);
						if (isSuccess === true) {
								this.props.handler();
						}
				}
		}, {
				key: "handleDeleteEvent",
				value: function handleDeleteEvent(eventId) {
						var isSuccess = deleteEvent(eventId);
						if (isSuccess === true) {
								this.props.handler();
						}
				}
		}, {
				key: "render",
				value: function render() {
						var _this8 = this;

						return React.createElement(
								"tr",
								null,
								React.createElement(
										"td",
										null,
										this.props.num
								),
								React.createElement(
										"td",
										null,
										this.props.name
								),
								React.createElement(
										"td",
										null,
										this.props.description
								),
								React.createElement(
										"td",
										null,
										this.props.time
								),
								React.createElement(
										"td",
										null,
										this.props.state
								),
								React.createElement(
										"td",
										null,
										this.props.priority
								),
								React.createElement(
										"td",
										null,
										React.createElement(
												"div",
												{ className: "d-flex flex-row" },
												React.createElement(
														"button",
														{ type: "button", className: "btn btn-success btn-sm me-2", title: "\u0417\u0430\u0432\u0435\u0440\u0448\u0438\u0442\u044C", onClick: function onClick() {
																		return _this8.handleFinishEvent(_this8.props.eventId);
																} },
														React.createElement(
																"svg",
																{ xmlns: "http://www.w3.org/2000/svg", width: "16", height: "16", fill: "currentColor", className: "bi bi-calendar-check", viewBox: "0 0 16 16" },
																React.createElement("path", { d: "M10.854 7.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 9.793l2.646-2.647a.5.5 0 0 1 .708 0z" }),
																React.createElement("path", { d: "M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" })
														)
												),
												React.createElement(
														"button",
														{ type: "button", className: "btn btn-secondary btn-sm me-2", title: "\u041E\u0442\u043C\u0435\u043D\u0438\u0442\u044C", onClick: function onClick() {
																		return _this8.handleCancelEvent(_this8.props.eventId);
																} },
														React.createElement(
																"svg",
																{ xmlns: "http://www.w3.org/2000/svg", width: "16", height: "16", fill: "currentColor", className: "bi bi-x-lg", viewBox: "0 0 16 16" },
																React.createElement("path", { fillRule: "evenodd", d: "M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z" }),
																React.createElement("path", { fillRule: "evenodd", d: "M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z" })
														)
												),
												React.createElement(
														"button",
														{ type: "button", className: "btn btn-danger btn-sm", title: "\u0423\u0434\u0430\u043B\u0438\u0442\u044C", onClick: function onClick() {
																		return _this8.handleDeleteEvent(_this8.props.eventId);
																} },
														React.createElement(
																"svg",
																{ xmlns: "http://www.w3.org/2000/svg", width: "16", height: "16", fill: "currentColor", className: "bi bi-trash", viewBox: "0 0 16 16" },
																React.createElement("path", { d: "M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" }),
																React.createElement("path", { fillRule: "evenodd", d: "M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" })
														)
												)
										)
								)
						);
				}
		}]);

		return EventEntityRow;
}(React.Component);

var AddEventForm = function (_React$Component7) {
		_inherits(AddEventForm, _React$Component7);

		function AddEventForm(props) {
				_classCallCheck(this, AddEventForm);

				return _possibleConstructorReturn(this, (AddEventForm.__proto__ || Object.getPrototypeOf(AddEventForm)).call(this, props));
		}

		_createClass(AddEventForm, [{
				key: "render",
				value: function render() {
						return React.createElement(
								"tr",
								{ id: "addEvent", className: "d-none" },
								React.createElement("td", null),
								React.createElement(
										"td",
										null,
										React.createElement("input", { type: "text", className: "form-control form-control-sm", name: "name", id: "eventFormName" })
								),
								React.createElement(
										"td",
										null,
										React.createElement("textarea", { className: "form-control form-control-sm", name: "description", id: "eventFormDescription" })
								),
								React.createElement(
										"td",
										null,
										React.createElement("input", { type: "time", className: "form-control form-control-sm", name: "time", id: "eventFormTime" })
								),
								React.createElement(
										"td",
										null,
										React.createElement(
												"select",
												{ className: "form-select form-select-sm", defaultValue: "TODO", name: "state" },
												React.createElement(
														"option",
														{ value: "TODO" },
														codeToValue("TODO")
												),
												React.createElement(
														"option",
														{ value: "CANCELED" },
														codeToValue("CANCELED")
												),
												React.createElement(
														"option",
														{ value: "DONE" },
														codeToValue("DONE")
												)
										)
								),
								React.createElement(
										"td",
										null,
										React.createElement(
												"select",
												{ className: "form-select form-select-sm", defaultValue: "HIGH", name: "priority" },
												React.createElement(
														"option",
														{ value: "HIGH" },
														codeToValue("HIGH")
												),
												React.createElement(
														"option",
														{ value: "MEDIUM" },
														codeToValue("MEDIUM")
												),
												React.createElement(
														"option",
														{ value: "LOW" },
														codeToValue("LOW")
												)
										)
								),
								React.createElement(
										"td",
										null,
										React.createElement(
												"button",
												{ type: "submit", title: "\u0421\u043E\u0445\u0440\u0430\u043D\u0438\u0442\u044C", className: "btn btn-primary btn-sm" },
												React.createElement(
														"svg",
														{ xmlns: "http://www.w3.org/2000/svg", width: "16", height: "16", fill: "currentColor", className: "bi bi-save", viewBox: "0 0 16 16" },
														React.createElement("path", { d: "M2 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9.5a1 1 0 0 0-1 1v7.293l2.646-2.647a.5.5 0 0 1 .708.708l-3.5 3.5a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L7.5 9.293V2a2 2 0 0 1 2-2H14a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h2.5a.5.5 0 0 1 0 1H2z" })
												)
										)
								)
						);
				}
		}]);

		return AddEventForm;
}(React.Component);

var EventsTable = function (_React$Component8) {
		_inherits(EventsTable, _React$Component8);

		function EventsTable(props) {
				_classCallCheck(this, EventsTable);

				var _this10 = _possibleConstructorReturn(this, (EventsTable.__proto__ || Object.getPrototypeOf(EventsTable)).call(this, props));

				_this10.handler = _this10.handler.bind(_this10);
				return _this10;
		}

		_createClass(EventsTable, [{
				key: "componentDidMount",
				value: function componentDidMount() {
						var eventsTableObj = this;
						$("#showAddEventFormButton").click(function () {
								showAddEventForm();
						});

						$("#hideAddEventFormButton").click(function () {
								hideAddEventForm();
								clearAddEventForm();
						});

						$("#addEventForm").validate({
								rules: {
										name: "required",
										state: "required",
										priority: "required"
								},
								messages: {
										name: "Введите имя",
										state: "Введите статус",
										priority: "Введите приоритет"
								},
								errorElement: "em",
								errorPlacement: function errorPlacement(error, element) {
										element.addClass("is-invalid");
										error.addClass("invalid-feedback");
										error.insertAfter(element);
								},
								highlight: function highlight(element, errorClass, validClass) {
										$(element).addClass("is-invalid").removeClass("is-valid");
								},
								unhighlight: function unhighlight(element, errorClass, validClass) {
										$(element).addClass("is-valid").removeClass("is-invalid");
								},
								submitHandler: function submitHandler(form) {
										var userId = null;
										var userInfo = getUserInfo();
										if (userInfo != null) {
												userId = userInfo.userId;
										}

										if (userId != null) {
												var _event = $(form).serializeJSON();
												_event.id = null;
												_event.userId = userId;
												_event.date = eventsTableObj.props.currentDate;
												emptyToNull(_event);

												var status = createEvent(_event);
												if (status === "success") {
														hideAddEventForm();
														clearAddEventForm();
														eventsTableObj.forceUpdate();
														showAlert("Событие успешно добавлено", "success");
												} else {
														showAlert("Ошибка при создании события: " + errorThrown, "danger");
												}
										}
								}
						});
				}
		}, {
				key: "handler",
				value: function handler() {
						this.forceUpdate();
				}
		}, {
				key: "render",
				value: function render() {
						var events = getUserEventsByDate(Cookies.get("userId"), this.props.currentDate);
						var eventRows = [];
						for (i in events) {
								eventRows.push(React.createElement(EventEntityRow, { handler: this.handler, key: events[i].id, num: parseInt(i) + 1, eventId: events[i].id, name: events[i].name, description: events[i].description, time: events[i].time === null ? null : events[i].time.slice(0, -3), state: codeToValue(events[i].state), priority: codeToValue(events[i].priority) }));
						}
						return React.createElement(
								"div",
								{ className: "cal-table mt-3" },
								React.createElement(
										"form",
										{ id: "addEventForm" },
										React.createElement(
												"table",
												{ className: "table" },
												React.createElement(
														"thead",
														null,
														React.createElement(
																"tr",
																null,
																React.createElement(
																		"th",
																		{ scope: "col" },
																		"\u041D\u043E\u043C\u0435\u0440"
																),
																React.createElement(
																		"th",
																		{ scope: "col" },
																		"\u041D\u0430\u0437\u0432\u0430\u043D\u0438\u0435"
																),
																React.createElement(
																		"th",
																		{ scope: "col" },
																		"\u041E\u043F\u0438\u0441\u0430\u043D\u0438\u0435"
																),
																React.createElement(
																		"th",
																		{ scope: "col" },
																		"\u0412\u0440\u0435\u043C\u044F"
																),
																React.createElement(
																		"th",
																		{ scope: "col" },
																		"\u0421\u0442\u0430\u0442\u0443\u0441"
																),
																React.createElement(
																		"th",
																		{ scope: "col" },
																		"\u041F\u0440\u0438\u043E\u0440\u0438\u0442\u0435\u0442"
																),
																React.createElement("th", { scope: "col" })
														)
												),
												React.createElement(
														"tbody",
														null,
														eventRows,
														React.createElement(AddEventForm, null)
												)
										)
								),
								React.createElement(
										"div",
										{ className: "text-center" },
										React.createElement(
												"button",
												{ type: "button", className: "btn btn-primary me-2", id: "showAddEventFormButton" },
												"\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C \u0441\u043E\u0431\u044B\u0442\u0438\u0435"
										),
										React.createElement(
												"button",
												{ type: "button", className: "btn btn-secondary d-none", id: "hideAddEventFormButton" },
												"\u041E\u0442\u043C\u0435\u043D\u0430"
										)
								)
						);
				}
		}]);

		return EventsTable;
}(React.Component);

var Calendar = function (_React$Component9) {
		_inherits(Calendar, _React$Component9);

		function Calendar(props) {
				_classCallCheck(this, Calendar);

				var _this11 = _possibleConstructorReturn(this, (Calendar.__proto__ || Object.getPrototypeOf(Calendar)).call(this, props));

				_this11.state = {
						currentDate: new Date().toDateInputValue()
				};
				_this11.handler = _this11.handler.bind(_this11);
				return _this11;
		}

		_createClass(Calendar, [{
				key: "handler",
				value: function handler(date) {
						this.setState({ currentDate: date });
				}
		}, {
				key: "render",
				value: function render() {
						return React.createElement(
								"div",
								null,
								React.createElement(DatePicker, { handler: this.handler }),
								React.createElement(EventsTable, { currentDate: this.state.currentDate })
						);
				}
		}]);

		return Calendar;
}(React.Component);

var PrimaryContent = function (_React$Component10) {
		_inherits(PrimaryContent, _React$Component10);

		function PrimaryContent(props) {
				_classCallCheck(this, PrimaryContent);

				var _this12 = _possibleConstructorReturn(this, (PrimaryContent.__proto__ || Object.getPrototypeOf(PrimaryContent)).call(this, props));

				_this12.changeCurrentUserName = _this12.changeCurrentUserName.bind(_this12);
				return _this12;
		}

		_createClass(PrimaryContent, [{
				key: "changeCurrentUserName",
				value: function changeCurrentUserName(name) {
						this.props.handler(name);
				}
		}, {
				key: "render",
				value: function render() {
						var content = void 0;
						if (this.props.activeMenuItem === "Ежедневник") {
								content = React.createElement(Calendar, null);
						} else {
								content = React.createElement(SettingsForm, { handler: this.changeCurrentUserName });
						}
						return React.createElement(
								"div",
								{ className: "container-fluid bg-light scrollarea" },
								React.createElement(PrimaryContentHeader, { activeMenuItem: this.props.activeMenuItem }),
								React.createElement(AlertDiv, null),
								content
						);
				}
		}]);

		return PrimaryContent;
}(React.Component);

var App = function (_React$Component11) {
		_inherits(App, _React$Component11);

		function App(props) {
				_classCallCheck(this, App);

				var _this13 = _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this, props));

				_this13.state = {
						activeMenuItem: "Ежедневник",
						activeUserName: ""
				};
				_this13.handler = _this13.handler.bind(_this13);
				_this13.changeCurrentUserName = _this13.changeCurrentUserName.bind(_this13);
				return _this13;
		}

		_createClass(App, [{
				key: "componentDidMount",
				value: function componentDidMount() {
						var userId = null;
						var userInfo = getUserInfo();
						if (userInfo != null) {
								userId = userInfo.userId;
								this.setState({ activeUserName: userInfo.lastName + " " + userInfo.firstName });
						}
						Cookies.set("userId", userId);
				}
		}, {
				key: "handler",
				value: function handler(menuName) {
						this.setState({ activeMenuItem: menuName });
				}
		}, {
				key: "changeCurrentUserName",
				value: function changeCurrentUserName(name) {
						this.setState({ activeUserName: name });
				}
		}, {
				key: "render",
				value: function render() {
						return React.createElement(
								"main",
								null,
								React.createElement(Navigation, { handler: this.handler, activeUserName: this.state.activeUserName }),
								React.createElement(NavigationDivider, null),
								React.createElement(PrimaryContent, { activeMenuItem: this.state.activeMenuItem, handler: this.changeCurrentUserName })
						);
				}
		}]);

		return App;
}(React.Component);

ReactDOM.render(React.createElement(App, null), document.getElementById('root'));