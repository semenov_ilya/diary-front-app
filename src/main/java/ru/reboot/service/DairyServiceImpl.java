package ru.reboot.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.reboot.dto.EventDTO;
import ru.reboot.dto.User;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCodes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import java.util.Arrays;
import java.util.List;
import java.util.*;

@Component
public class DairyServiceImpl implements DiaryService {

    private static final Logger logger = LogManager.getLogger(DairyServiceImpl.class);

    @Value("${client.auth-service}")
    private String authService;

    @Value("${client.event-service}")
    private String eventService;

    @Value("${client.phone-service}")
    private String phoneService;

    private RestTemplate restTemplate;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public User authorizeUser(AuthorizeDTO authorizeDTO) {
        logger.info("Method DairyService.authorizeUser");
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(authorizeDTO.getLogin(), authorizeDTO.getPassword());

        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        User user = this.getUserByLogin(authorizeDTO.getLogin());
        logger.info("Method DairyService.authorizeUser completed user={}", user);
        return user;
    }

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        logger.info("Method DairyService.logout");

        try {
            request.logout();
            response.sendRedirect("info");
        } catch (ServletException | IOException exception) {
            String logMessage = "Failed to .logout error={}" + exception;
            logger.error(logMessage, exception);
            String errorCodeMessage = ErrorCodes.LOGOUT_ERROR.toString();
            throw new BusinessLogicException(errorCodeMessage, exception, errorCodeMessage);
        }

        logger.info("Method DairyService.logout completed");
    }

    @Override
    public User getUserInfo() {
        logger.info("Method DairyService.getUserInfo");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (!authentication.isAuthenticated()) {
            String error = ErrorCodes.UNAUTHORISED.toString();
            RuntimeException exception = new BusinessLogicException(error, error);
            String logMessage = "Failed to .getUserInfo error={}" + exception;
            logger.error(logMessage, exception);
            throw exception;
        }

        String login = authentication.getName();
        User user = this.getUserByLogin(login);

        logger.info("Method DairyService.getUserInfo completed user={}", user);
        return user;
    }

    @Override
    public User registerUser(User user) {
        logger.info("Method DiaryService.registerUser started");
        String url = authService + "/auth/user";
        try {
            User newUser = restTemplate.postForObject(url, user, User.class);
            logger.info("Method DiaryService.registerUser completed newUser={}", newUser);
            return newUser;
        } catch (Exception e) {
            throw new BusinessLogicException(e.getMessage(), e, ErrorCodes.REGISTER_USER_ERROR.toString());
        }

    }

    @Override
    public User updateUser(User user) {
        logger.info("Method DiaryService.updateUser started");
        String url = authService + "/auth/user";
        try {
            HttpEntity<User> httpEntity = new HttpEntity<User>(user);
            User newUser = restTemplate.exchange(url, HttpMethod.PUT, httpEntity, User.class).getBody();
            logger.info("Method DiaryService.updateUser completed user={}", newUser);
            return newUser;
        } catch (Exception e) {
            throw new BusinessLogicException(e.getMessage(), e, ErrorCodes.UPDATE_USER_ERROR.toString());
        }
    }

    @Override
    public User getUserByLogin(String login) {
        logger.info("Method DairyService.getUserByLogin login={}", login);

        final String additionalUrl = "/auth/user/byLogin?login={login}";
        final String targetUrl = authService + additionalUrl;

        User user = restTemplate.getForEntity(targetUrl, User.class, login).getBody();
        logger.info("Method DairyService.getUserByLogin completed user={}", user);
        return user;
    }

    @Override
    public List<EventDTO> getUserEventsByDate(String userId, String date) {
        logger.info("Method DairyService.getUserEventsByDate userId={}, date={}", userId, date);
        String url = String.format(eventService + "/event/all/byDate?userId=%s&date=%s", userId, date);
        List<EventDTO> list = Arrays.asList(restTemplate.getForObject(url, EventDTO[].class));
        logger.info("Method DairyService.getUserEventsByDate completed list<EventDTO>={}", list);
        return list;
    }

    @Override
    public EventDTO deleteEvent(String eventId) {
        logger.info("Method DairyService.deleteEvent eventId={}", eventId);
        String url = String.format(eventService + "/event/%s", eventId);
        EventDTO eventDTO = restTemplate.exchange(url, HttpMethod.DELETE, null, EventDTO.class).getBody();
        logger.info("Method DairyService.deleteEvent competed eventDto={}", eventDTO);
        return eventDTO;
    }

    @Override
    public EventDTO cancelEvent(String eventId) {
        logger.info("Method .cancelEvent eventId={}", eventId);

        String url = String.format(eventService + "/event/%s/cancel", eventId);
        EventDTO event = restTemplate.exchange(url, HttpMethod.PUT, null, EventDTO.class).getBody();
        logger.info("Method .cancelEvent completed eventId={}", eventId);
        return event;
    }

    @Override
    public EventDTO finishEvent(String eventId) {
        logger.info("Method .finishEvent eventId={}", eventId);

        String url = String.format(eventService + "/event/%s/finish", eventId);
        EventDTO event = restTemplate.exchange(url, HttpMethod.PUT, null, EventDTO.class).getBody();
        logger.info("Method .finishEvent completed eventId={}", eventId);
        return event;
    }

    @Override
    public EventDTO createEvent(EventDTO event) {
        logger.info("Method DairyService.createEvent event={}", event);
        String url = eventService + "/event";
        EventDTO eventDTO = restTemplate.postForObject(url, event, EventDTO.class);
        logger.info("Method DairyService.createEvent competed event={}", event);
        return eventDTO;
    }

    @Override
    public String generateTelegramCode(String userId) {
        String targetUrl = String.format(phoneService + "/phone/telegram/generateCode?userId=%s", userId);
        try {
            return restTemplate.exchange(targetUrl, HttpMethod.POST, null, String.class).getBody();
        } catch (Exception e) {
            throw new BusinessLogicException(e.getMessage(), e, ErrorCodes.GENERATE_CODE_ERROR.toString());
        }

    }
}
