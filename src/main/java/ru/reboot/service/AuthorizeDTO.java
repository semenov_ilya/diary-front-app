package ru.reboot.service;

public class AuthorizeDTO {

    private String login;
    private String password;

    public AuthorizeDTO() {
    }

    public AuthorizeDTO(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
