package ru.reboot.service;

import ru.reboot.dto.EventDTO;
import ru.reboot.dto.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface DiaryService {

    /**
     * Авторизация пользователя
     */
    User authorizeUser(AuthorizeDTO authorizeDTO);

    /**
     * Выход пользователя
     */
    void logout(HttpServletRequest request , HttpServletResponse response);

    /**
     * Получить информацию о текущем залогиненом пользователе.
     * Взять из SecurityContext
     */
    User getUserInfo();

    /**
     * Создать нового пользователя
     */
    User registerUser(User user);

    /**
     * Обновить информацию о существующем пользователе
     */
    User updateUser(User user);


    User getUserByLogin(String login);
    /**
     * Получить список всех событий пользователя за определенную дату
     *
     * @param userId - user id
     */
    List<EventDTO> getUserEventsByDate(String userId, String date);

    /**
     * Удалить информацию о событии.
     *
     * @param eventId - event id
     */
    EventDTO deleteEvent(String eventId);

    /**
     * Отменить событие.
     *
     * @param eventId - event id
     */
    EventDTO cancelEvent(String eventId);

    /**
     * Завершить событие.
     *
     * @param eventId - event id
     */
    EventDTO finishEvent(String eventId);

    /**
     * Создать новое событие
     *
     * @param event - сохранить событие
     */
    EventDTO createEvent(EventDTO event);

    /**
     * Generate telegram code for current user.
     */
    String generateTelegramCode(String userId);
}
