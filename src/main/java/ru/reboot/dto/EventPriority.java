package ru.reboot.dto;

/**
 * Event priority.
 */
public enum EventPriority {

    HIGH,
    MEDIUM,
    LOW
}
