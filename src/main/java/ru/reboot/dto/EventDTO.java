package ru.reboot.dto;

import java.time.LocalDate;
import java.time.LocalTime;

public class EventDTO {

    private String id;
    private String userId;
    private String name;
    private String description;
    private LocalDate date;
    private LocalTime time;
    private EventState state;
    private EventPriority priority;

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public EventState getState() {
        return state;
    }

    public void setState(EventState state) {
        this.state = state;
    }

    public EventPriority getPriority() {
        return priority;
    }

    public void setPriority(EventPriority priority) {
        this.priority = priority;
    }

    private static class Builder {

        private EventDTO dto = new EventDTO();

        private Builder() {
            // private constructor
        }

        public Builder setId(String id) {
            dto.id = id;
            return this;
        }

        public Builder setUserId(String userId) {
            dto.userId = userId;
            return this;
        }

        public Builder setName(String name) {
            dto.name = name;
            return this;
        }

        public Builder setDescription(String description) {
            dto.description = description;
            return this;
        }

        public Builder setDate(LocalDate date) {
            dto.date = date;
            return this;
        }

        public Builder setTime(LocalTime time) {
            dto.time = time;
            return this;
        }

        public Builder setState(EventState state) {
            dto.state = state;
            return this;
        }

        public Builder setPriority(EventPriority priority) {
            dto.priority = priority;
            return this;
        }


        public EventDTO build() {
            return dto;
        }

    }
}
