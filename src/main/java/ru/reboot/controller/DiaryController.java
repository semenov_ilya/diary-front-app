package ru.reboot.controller;

import ru.reboot.dto.EventDTO;
import ru.reboot.dto.User;
import ru.reboot.service.AuthorizeDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface DiaryController {

    /**
     * Generate telegram code for user
     */
    String generateTelegramCode(String userId);

    /**
     * Получить информацию о текущем залогиненом пользователе.
     * Взять из SecurityContext
     */
    User getUserInfo();

    /**
     * Создать нового пользователя
     */
    User registerUser(User user);

    /**
     * Авторизация пользователя
     */
    void authorizeUser(AuthorizeDTO authorizeDTO, HttpServletResponse response) throws Exception;

    /**
     * Выход пользователя
     */
    void logout(HttpServletRequest request , HttpServletResponse response);

    /**
     * Обновить информацию о существующем пользователе
     */
    User updateUser(User user);

    /**
     * Создать новое событие
     */
    EventDTO createEvent(EventDTO event);

    /**
     * Получить список всех событий пользователя за определенную дату
     *
     * @param userId - user id
     */
    List<EventDTO> getUserEventsByDate(String userId, String date);

    /**
     * Удалить информацию о событии.
     *
     * @param eventId - event id
     */
    EventDTO deleteEvent(String eventId);

    /**
     * Отменить событие.
     *
     * @param eventId - event id
     */
    EventDTO cancelEvent(String eventId);

    /**
     * Завершить событие.
     *
     * @param eventId - event id
     */
    EventDTO finishEvent(String eventId);
}
