package ru.reboot.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.reboot.dto.EventDTO;
import ru.reboot.dto.User;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCodes;
import ru.reboot.service.AuthorizeDTO;
import ru.reboot.service.DiaryService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User controller.
 */
@RestController
@RequestMapping(path = "diary")
public class DiaryControllerImpl implements DiaryController {

    private static final Logger logger = LogManager.getLogger(DiaryControllerImpl.class);

    private DiaryService diaryService;

    @Autowired
    public void setDiaryService(DiaryService diaryService) {
        this.diaryService = diaryService;
    }

    @GetMapping("info")
    public String info() {
        logger.info("method .info invoked");
        return "Diary controller " + new Date();
    }

    @Override
    @PostMapping("auth/telegram/generateCode")
    public String generateTelegramCode(@RequestParam("userId") String userId) {
        logger.info("method .generateTelegramCode invoked");
        return diaryService.generateTelegramCode(userId);
    }

    @Override
    @GetMapping("auth/info")
    public User getUserInfo() {
        logger.info("Method DiaryController.getUserInfo");
        User user = diaryService.getUserInfo();
        logger.info("Method DiaryController.getUserByLogin completed user={}" , user);
        return user;
    }

    @Override
    @PostMapping("auth/authorize")
    public void authorizeUser(@RequestBody AuthorizeDTO authorizeDTO,
                              HttpServletResponse response) throws Exception {
        diaryService.authorizeUser(authorizeDTO);
    }

    @Override
    @GetMapping("logout")
    public void logout( HttpServletRequest request , HttpServletResponse response) {
        diaryService.logout(request ,response);
    }

    @Override
    @PostMapping("auth/user")
    public User registerUser(@RequestBody User user) {
        logger.info("Method DiaryControllerImpl.registerUser started");
        return diaryService.registerUser(user);
    }

    @Override
    @PutMapping("auth/user")
    public User updateUser(@RequestBody User user) {
        logger.info("Method DiaryControllerImpl.updateUser started");
        return diaryService.updateUser(user);
    }

    @Override
    @PostMapping("event")
    public EventDTO createEvent(@RequestBody EventDTO event) {
        logger.info("Method DairyController.createEvent event={}", event);
        try{
            EventDTO eventDTO = diaryService.createEvent(event);
            logger.info("Method DairyController.createEvent completed eventDTO={}", eventDTO);
            return eventDTO;
        } catch (Exception e){
            logger.error("Create event error");
            throw new BusinessLogicException("Error createEvent", e, ErrorCodes.CREATE_EVENT_ERROR.toString());
        }
    }

    @Override
    @GetMapping("event/all")
    public List<EventDTO> getUserEventsByDate(@RequestParam("userId") String userId,
                                              @RequestParam("date") String date) {
        logger.info("Method DairyController.getUserEventsByDate userId={}, date={}", userId, date);
        try {
            List<EventDTO> list = diaryService.getUserEventsByDate(userId, date);
            logger.info("Method DairyController.getUserEventsByDate completed userId={}, date={}", userId, date);
            return list;
        } catch (Exception e) {
            logger.error("Events not found");
            throw new BusinessLogicException("Events not found", e, ErrorCodes.EVENT_NOT_FOUND.toString());
        }
    }

    @Override
    @DeleteMapping("event/{eventId}")
    public EventDTO deleteEvent(@PathVariable("eventId") String eventId) {
        logger.info("Method DairyController.deleteEvent eventId={}", eventId);
        try {
            EventDTO eventDTO = diaryService.deleteEvent(eventId);
            logger.info("Method DairyController.deleteEvent completed eventDTO={}", eventDTO);
            return eventDTO;
        } catch (Exception e){
            logger.error("Events not found");
            throw new BusinessLogicException("Events not found", e, ErrorCodes.EVENT_NOT_FOUND.toString());
        }
    }

    @Override
    @PutMapping("event/{eventId}/cancel")
    public EventDTO cancelEvent(@PathVariable("eventId") String eventId) {
        logger.info("method .cancelEvent invoked");
        try {
            EventDTO event = diaryService.cancelEvent(eventId);
            logger.info("Method DairyController.cancelEvent completed eventDTO={}", event);
            return event;
        } catch (Exception e){
            logger.error("Event not found");
            throw new BusinessLogicException("Event not found", e, ErrorCodes.EVENT_NOT_FOUND.toString());
        }
    }

    @Override
    @PutMapping("event/{eventId}/finish")
    public EventDTO finishEvent(@PathVariable("eventId") String eventId) {
        logger.info("method .finishEvent invoked");
        try {
            EventDTO event = diaryService.finishEvent(eventId);
            logger.info("Method DairyController.finishEvent completed eventDTO={}", event);
            return event;
        } catch (Exception e){
            logger.error("Event not found");
            throw new BusinessLogicException("Event not found", e, ErrorCodes.EVENT_NOT_FOUND.toString());
        }
    }

    @ExceptionHandler
    public ResponseEntity<Map> handle(Exception ex) {

        Map<String, String> fields = new HashMap<>();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(fields);
    }
}
