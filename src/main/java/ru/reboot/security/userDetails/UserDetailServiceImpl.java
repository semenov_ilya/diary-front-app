package ru.reboot.security.userDetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.reboot.dto.User;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.service.DiaryService;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private DiaryService diaryService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = null;

        try {
            user = diaryService.getUserByLogin(s);
        } catch (BusinessLogicException e) {
            throw new UsernameNotFoundException(e.getMessage(), e);
        }

        return new UserDetailImpl(user);
    }
}
